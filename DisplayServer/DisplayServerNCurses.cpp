#include "DisplayServerNCurses.hpp"

static NCursesDisplayDevice g_Dsp;
static int stdBgCol = COLOR_BLACK;

long To3BitColor( long color)
{
	if( color == -1 )
		return stdBgCol;
	if( color < 16 )
	{
		if(color > 8)
			color-=8;
		return color;
	}
	unsigned long newCol = 0;
	if( (color&0xFF0000) >= 0x7F0000 )
		newCol|=1;
	if( (color&0x00FF00) >= 0x7F00 )
		newCol|=2;
	if( (color&0x0000FF) >= 0x7F )
		newCol|=4;

	return newCol;
}

long To8BitColor( long color )
{
	if( color == -1 )
		return stdBgCol;
	if( color < 16 )
		return color;

	//Color cube for 256 colors consits of: 0..5 r, 0..5 g, 0..5 b
	long newCol = ((((color>>16)&0xFF)*5)/0xFF);
	newCol = newCol*6 + ((((color>>8)&0xFF)*5)/0xFF);
	newCol = newCol*6 + (((color&0xFF)*5)/0xFF) + 16;
	return newCol;
}


IDisplay* IDisplay::GetPrimaryDisplay()
{
	return &g_Dsp;
}

void NCursesDisplayDevice::InitialiseDisplay( )
{
	initscr();
	getmaxyx( stdscr, _YSize, _XSize );
	start_color();
	
	if(use_default_colors()==OK)
		stdBgCol = -1;

	_ColorPairMap = new int[COLORS*(COLORS+1)];
	for(int i = 0; i < COLORS*COLORS;i++)
		_ColorPairMap[i] = 0;
	
	ConvertColor = (COLORS==8)?To3BitColor:To8BitColor;

	_MaxColPair = 1;	
	refresh();
}


unsigned long NCursesDisplayDevice::GetColorVal(long fgCol, long bgCol)
{
	fgCol = ConvertColor(fgCol);
	bgCol = ConvertColor(bgCol);
	unsigned long index;
	if( bgCol == -1 )
		index = fgCol*COLORS+COLORS;
	else
		index = fgCol*COLORS+bgCol;

	if( _ColorPairMap[ index ] != 0 )
	{
		return COLOR_PAIR(_ColorPairMap[index]);
	}

	if( _MaxColPair < COLOR_PAIRS )
	{
		init_pair( _MaxColPair, fgCol, bgCol );
		_ColorPairMap[ index ] = _MaxColPair;
		++_MaxColPair;
		return COLOR_PAIR(_ColorPairMap[index]);
	}
	throw -1;
	return 0;
}

IWindow *NCursesDisplayDevice::ConstructWindow(unsigned long xpos, unsigned long ypos, unsigned long xsize, unsigned long ysize)
{
	return  new NCursesWindow( newwin(ysize,xsize,ypos,xpos),xpos, ypos, xsize, ysize );
}

NCursesDisplayDevice::NCursesDisplayDevice()
{
}

NCursesDisplayDevice::~NCursesDisplayDevice()
{
	if( _ColorPairMap )
	{
		delete []_ColorPairMap;
		_ColorPairMap = nullptr;
	}	

	endwin();
}

void NCursesDisplayDevice::GetScreenDimensionsInChars(unsigned long &x, unsigned long &y)
{
	x = _XSize;
	y = _YSize;
}









//NCurses Window initialisation!

NCursesWindow::NCursesWindow( 	WINDOW *Handle, unsigned long absPosX, unsigned long absPosY, 
				unsigned long XSize, unsigned long YSize ) : 	
				_absXPos( absPosX ),  
				_absYPos( absPosY ),
				_XSize( XSize ),
				_YSize( YSize ),
				_wHandle(Handle),
				_lastBgAttr(0),
				_lastFgAttr(7)
{

}

NCursesWindow::~NCursesWindow()
{
	for( auto &i : _childWindows )
	{
		delete i;
	}
	_childWindows.clear();

	if(_wHandle)
	{
		delwin(_wHandle);
		_wHandle = nullptr;
	}
}


void NCursesWindow::SetTextColor(long fgColor, long bgColor)
{
	_lastFgAttr = fgColor;
	_lastBgAttr = bgColor;
	wattrset( _wHandle, g_Dsp.GetColorVal(fgColor,bgColor));
} 

void NCursesWindow::GetCursorPosition(unsigned long &x, unsigned long &y)
{
	getyx( _wHandle, y, x);
} 

void NCursesWindow::SetCursorPosition( const unsigned long x, const unsigned long y)
{
	wmove( _wHandle, y, x);
} 

void NCursesWindow::DrawString(const char *str)
{
	waddstr( _wHandle, str );
} 


void NCursesWindow::DrawString(const char *str, unsigned long numCharacters)
{
	waddnstr( _wHandle, str, numCharacters );
} 

void NCursesWindow::Refresh()
{
	wrefresh( _wHandle );
	for( auto &i : _childWindows )
		i->Refresh();
} 

void NCursesWindow::InsertChar(char x)
{
	winsch(_wHandle,x);
} 


void NCursesWindow::InsertString( const char *str)
{
	winsstr(_wHandle,str);
}
 
void NCursesWindow::InsertString(const char *str, unsigned long numChars)
{
	winsnstr( _wHandle, str, numChars);
}
 
void NCursesWindow::SetWindowFocus(bool ShowFg)
{
	Refresh();
} 

void NCursesWindow::ClearWindow()
{
	wclear(_wHandle);
	wbkgd( _wHandle, g_Dsp.GetColorVal( _lastFgAttr, _lastBgAttr) );
	for( auto &i : _childWindows )
		i->ClearWindow();
} 

IWindow *NCursesWindow::SplitWindowVert(unsigned long nLines)
{
	return nullptr;
}
 
IWindow *NCursesWindow::SplitWindowHor(unsigned long nLines)
{
	_YSize -= nLines;
	wresize( _wHandle, _YSize-nLines, _XSize );
	return g_Dsp.ConstructWindow( _absXPos, _YSize, _XSize, nLines );
} 
	
//There are a few reserved uuids, which are defined in the WindowUuid enum.
//It defines all reserved types of window uuids and what they are used for
void NCursesWindow::AddChildWindow( IWindow* wHandle,unsigned long uuid)
{
	for( auto &i : _childUuids )
	{
		if( i == uuid )
			throw 0;
	}
	_childUuids.push_back(uuid);
	_childWindows.push_back(wHandle);
}
 
IWindow *NCursesWindow::GetChildWindow(unsigned long uuid)
{
	unsigned long i = 0;
	for( auto &ii : _childUuids )
	{
		if( ii == uuid )
			return _childWindows[i];
		i++;
	}
	return nullptr;
} 
	

