#include "DisplayServer.hpp"

#include <unistd.h>

using namespace CONSOLE_COLORS;

int main()
{
	IDisplay *dev = IDisplay::GetPrimaryDisplay();
	dev->InitialiseDisplay();
	unsigned long x,y;
	dev->GetScreenDimensionsInChars( x, y );

	IWindow *win = dev->ConstructWindow( 0, 0, x, y);
	win->SetTextColor( COLOR_RED, COLOR_DEFAULT );
	win->ClearWindow();
	win->DrawString("Hello World");
	win->Refresh();

	IWindow *dspLine = win->SplitWindowHor( 1);
	win->AddChildWindow( dspLine, WINDOW_STATELINE );

	dspLine->SetTextColor( COLOR_BLACK, COLOR_WHITE );
	dspLine->ClearWindow();
	dspLine->DrawString("$>");
	dspLine->Refresh();


	sleep(5);

	return 0;
}
