#ifndef _PARALLEL_ATOMIC_DISPLAY_SERVER_NCURESES_IMP_HPP
#define _PARALLEL_ATOMIC_DISPLAY_SERVER_NCURESES_IMP_HPP

#include "DisplayServer.hpp"

#include <ncurses.h>
#include <vector>

class NCursesWindow : public IWindow
{
	protected:
		unsigned long _absXPos, _absYPos;
		unsigned long _XSize, _YSize;
		WINDOW *_wHandle;
		unsigned long _lastBgAttr;
		unsigned long _lastFgAttr;
		std::vector<IWindow*> _childWindows;
		std::vector<unsigned long> _childUuids;

	public:
		NCursesWindow( WINDOW *Handle, unsigned long absPosX, unsigned long absPosY, unsigned long XSize, unsigned long YSize );

		virtual void SetTextColor(long fgColor, long bgColor) override;
		virtual void GetCursorPosition(unsigned long &x, unsigned long &y) override;
		virtual void SetCursorPosition( const unsigned long x, const unsigned long y) override;
		virtual void DrawString(const char *str) override;
		virtual void DrawString(const char *str, unsigned long numCharacters) override;
		virtual void Refresh() override;
		virtual void InsertChar(char x) override;
		virtual void InsertString( const char *str) override;
		virtual void InsertString(const char *str, unsigned long numChars) override;
		virtual void SetWindowFocus(bool ShowFg) override;

		virtual void ClearWindow() override;
		virtual IWindow *SplitWindowVert(unsigned long nLines) override;
		virtual IWindow *SplitWindowHor(unsigned long nLines) override;
	
		//There are a few reserved uuids, which are defined in the WindowUuid enum.
		//It defines all reserved types of window uuids and what they are used for
		virtual void AddChildWindow( IWindow* wHandle,unsigned long uuid) override; 
		virtual IWindow *GetChildWindow(unsigned long uuid) override;
	
		//Clears the window to the current background color
		~NCursesWindow();
};



class NCursesDisplayDevice : public IDisplay
{
	private:		
		unsigned long _XSize, _YSize;
		int *_ColorPairMap;
		int _MaxColPair;
		long (*ConvertColor)(long);

	public:
		unsigned long GetColorVal(long bgCol,long fgCol);
	
		virtual void GetScreenDimensionsInChars( unsigned long &x, unsigned long &y) override;
	
		//xpos,ypos,xsize,ysize are expected to be given as character sizes.
		//For example xsize = 5 means the window will be 5 characters width
		virtual IWindow *ConstructWindow( unsigned long xpos, unsigned long ypos, unsigned long xsize, unsigned long ysize) override;
	
		virtual void InitialiseDisplay() override;


		~NCursesDisplayDevice();
		NCursesDisplayDevice();
};


#endif
