#ifndef _PARALLEL_ATOMIC_DISPLAY_SERVER_HPP_
#define _PARALLEL_ATOMIC_DISPLAY_SERVER_HPP_

constexpr unsigned long CREATE_COLOR32( unsigned char r, unsigned char g, unsigned char b )
{
	return (r<<16)|(g<<8)|b;
};


namespace CONSOLE_COLORS
{
	enum
	{
		COLOR_DEFAULT = -1,
		COLOR_BLACK = 0,
		COLOR_RED = 1,
		COLOR_GREEN = 2,
		COLOR_YELLOW = 3,
		COLOR_BLUE = 4,
		COLOR_MAGENTA = 5,
		COLOR_CYAN = 6,
		COLOR_WHITE = 7
	};

	enum Attributes
	{
		PA_BOLD = 1,
		PA_UNDERLINE = 2,
	};
};

enum WindowUuid
{
//This child window will output all state information for the user
//, like on which file he is, if he has changed the file, maybe the time 
//and things like that
	WINDOW_STATELINE = 0,
	WINDOW_ERR = -1
};

class IWindow
{
	public:
		virtual void SetTextColor(long fgColor, long bgColor) = 0;
		virtual void GetCursorPosition(unsigned long &x, unsigned long &y) = 0;
		virtual void SetCursorPosition( const unsigned long x, const unsigned long y) = 0;
		virtual void DrawString(const char *str) = 0;
		virtual void DrawString(const char *str, unsigned long numCharacters) = 0;
		virtual void Refresh() = 0;
		virtual void InsertChar(char x) = 0;
		virtual void InsertString( const char *str) = 0;
		virtual void InsertString(const char *str, unsigned long numChars) = 0;
		virtual void SetWindowFocus(bool ShowFg) = 0;

		virtual void ClearWindow() = 0;
		virtual IWindow *SplitWindowVert(unsigned long nLines) = 0;
		virtual IWindow *SplitWindowHor(unsigned long nLines) = 0;
	
		//There are a few reserved uuids, which are defined in the WindowUuid enum.
		//It defines all reserved types of window uuids and what they are used for
		virtual void AddChildWindow( IWindow* wHandle,unsigned long uuid) = 0; 
		virtual IWindow *GetChildWindow(unsigned long uuid) = 0;
	
		//Clears the window to the current background color
		virtual ~IWindow(){}
};


class IDisplay
{
	public:
		virtual void GetScreenDimensionsInChars( unsigned long &x, unsigned long &y) = 0;
	
		//xpos,ypos,xsize,ysize are expected to be given as character sizes.
		//For example xsize = 5 means the window will be 5 characters width
		virtual IWindow *ConstructWindow( unsigned long xpos, unsigned long ypos, unsigned long xsize, unsigned long ysize) = 0;
	
		virtual void InitialiseDisplay() = 0;
		static IDisplay *GetPrimaryDisplay();
};
#endif
