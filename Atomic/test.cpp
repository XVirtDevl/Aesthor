#include "ARessource.hpp"
#include "Command.hpp"
#include "../Metaprogramming/tuple.hpp"
#include <iostream>
#include <unistd.h>
#include <thread>


class X
{
	private:
		AMeta::Tupel<int,int> xmy;
	public:
	X(int x, int y) : xmy(x,y){}
	void lol(int x,char yol, int X, int Yolo)
	{
		std::cout<<x<<std::endl;
		std::cout<<"Works perfectly: "<<yol<<std::endl;
		std::cout<<"Yolo: "<<Yolo<<std::endl;
	}
};



int main()
{

	int x = 100;

	AAtomic::Ressource<int*>* ResPtr = new AAtomic::Ressource<int*>(&x);

	AAtomic::RessourceAccessPipeline<int*> brdg(ResPtr);

	std::cout<<AMeta::VariadicArgumentCount<int,char,bool>::size<<std::endl;	

	

	AMeta::Tupel<int,char,int,int> myTup(10,'!',10000,2000);
	
	X callee(200,300);

	AMeta::UnrollThings<4>::Unroll( &callee, &X::lol, myTup);


	return 0;
}
