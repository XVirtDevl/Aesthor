#ifndef _AESTHOR_RESSOURCE_HPP_
#define _AESTHOR_RESSOURCE_HPP_

#include <mutex>
#include <type_traits>
#include <atomic>
#include "../Metaprogramming/meta.hpp"
#include <iostream>

namespace AAtomic
{

	enum class LockType
	{
		LOCK_FOR_READ,
		LOCK_FOR_WRITE,
		LOCK_INVALID
	};
	
	template<class R>class RessourceAccessPipeline;

	template<typename ContainedType>class Ressource
	{
		private:
			typedef typename AMeta::Conditional<AMeta::IsPointer<ContainedType>::value, ContainedType,void>::Type ContainerT;;
			std::atomic<unsigned long> _numReads;
			std::mutex _writeLock;
			
			ContainerT _res;
	
			bool TrySetLock( LockType lock );
			void SetLock( LockType lock);
			void Unlock( LockType lock );

			ContainerT GetRessource();
		public:	
			void SetRessource(ContainedType res)
			{
				_res = res;
				_numReads.store(0);
			}
			
			Ressource( ContainedType _cont ) : _numReads(0),_res(_cont){}
			Ressource() : _numReads(0),_res(nullptr){}	
			Ressource<ContainedType> *CopyRessource();
			friend class RessourceAccessPipeline<ContainerT>;
	};

	template<typename AccessType>class RessourceAccessPipeline
	{
		private:
			Ressource<AccessType> *_ressource;
			LockType _lastLockType;

		public:
			RessourceAccessPipeline( Ressource<AccessType> *_res ) : _ressource(_res),_lastLockType(LockType::LOCK_INVALID){};
			~RessourceAccessPipeline(){Unlock();};

			AccessType GetRessource();
			bool TrySetLock( LockType lock );
			void SetLock( LockType lock );
			void Unlock();	
	};

};


#include "ARessource.cpp"

#endif
