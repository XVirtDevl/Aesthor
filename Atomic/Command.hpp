#ifndef _AESTHOR_ATOMIC_COMMANDS_HPP_
#define _AESTHOR_ATOMIC_COMMANDS_HPP_

#include <mutex>




namespace AAtomic
{
	class Command
	{
		protected:
			std::mutex _FinishedJob;

		public:	
			Command()
			{
				_FinishedJob.lock(); 
			}

			virtual ~Command()
			{
				_FinishedJob.unlock();
			}

			void WaitForJobFinish()
			{
				_FinishedJob.lock();
			}

			virtual void Execute() = 0;
			virtual void Undo() = 0;
	};
};


#endif
