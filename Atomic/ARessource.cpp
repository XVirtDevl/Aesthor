template<typename T>
bool AAtomic::Ressource<T>::TrySetLock( AAtomic::LockType lock )
{
	if( lock == AAtomic::LockType::LOCK_FOR_READ)
	{
		if( _writeLock.try_lock() )
		{
			_numReads.fetch_add(1);
			_writeLock.unlock();
			return true;
		}
	}
	else
	{
		if(_writeLock.try_lock())
		{
			if(_numReads.load() == 0)
				return true;
			_writeLock.unlock();
		}	
	
	}
	return false;
}

template<typename T>
void AAtomic::Ressource<T>::SetLock( AAtomic::LockType lock )
{
	_writeLock.lock();
	if( lock == AAtomic::LockType::LOCK_FOR_READ)
	{
		_numReads.fetch_add(1);
		_writeLock.unlock();
		return;
	}
	else
	{
		for(;;)
			if(_numReads.load() == 0)
				return;
	}
}

template<typename T>
void AAtomic::Ressource<T>::Unlock( AAtomic::LockType lock )
{
	if( lock == AAtomic::LockType::LOCK_FOR_READ )
	{
		_numReads.fetch_sub(1);
	}
	else
	{
		_writeLock.unlock();
	}
}

template<typename T> AAtomic::Ressource<T> *AAtomic::Ressource<T>::CopyRessource()
{
	return nullptr;
}


template<typename T>typename AAtomic::Ressource<T>::ContainerT AAtomic::Ressource<T>::GetRessource()
{
	return _res;	
}




template<typename T>
T AAtomic::RessourceAccessPipeline<T>:: GetRessource()
{
	if( _lastLockType == AAtomic::LockType::LOCK_INVALID)
		throw 0;
	return _ressource->GetRessource();
}

template<typename T>
bool AAtomic::RessourceAccessPipeline<T>::TrySetLock( AAtomic::LockType lock )
{
	if(_ressource->TrySetLock(lock) == false)
		return false;
	
	_lastLockType = lock;
	return true;		
}

template<typename T>
void AAtomic::RessourceAccessPipeline<T>::SetLock( AAtomic::LockType lock )
{
	_lastLockType = lock;
	_ressource->SetLock(lock);
}

template<typename T>
void AAtomic::RessourceAccessPipeline<T>::Unlock()
{
	if( _lastLockType != AAtomic::LockType::LOCK_INVALID )
		_ressource->Unlock(_lastLockType);
	_lastLockType = AAtomic::LockType::LOCK_INVALID;
}
