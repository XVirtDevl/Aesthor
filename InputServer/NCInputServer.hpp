#ifndef _NCURSES_INPUT_SERVER_HPP_
#define _NCURSES_INPUT_SERVER_HPP_

#include <ncurses.h>
#include "InputServer.hpp"


class NCursesInputServer : public IInputServer
{
	public:
		void Start() override;
};


#endif
