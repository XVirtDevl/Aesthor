#ifndef _INPUT_SERVER_HPP
#define _INPUT_SERVER_HPP

class IInputServer
{
	public:
		static IInputServer *CreateInputServer();
		virtual void Start() = 0;
};


#endif
