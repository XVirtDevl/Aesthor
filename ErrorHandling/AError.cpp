#include "AError.hpp"



using namespace AError;



const char *FatalError::g_pLogfileName = "error.log";


void FatalError::Initialise( const char *pErrorCause )
{	
	_error += pErrorCause;

	if( g_WriteErrorsToLogfile )
	{
		std::ofstream ofs( g_pLogfileName, std::ios::app|std::ios::out|std::ios::ate);
		time_t currT = time(0);
		tm *curr = localtime( &currT );

		ofs<<"Statistics : Fatal error ( date: ";
		ofs<<(curr->tm_mday)<<'.';
		ofs<<(curr->tm_mon + 1)<<'.';
		ofs<<(curr->tm_year+1900)<<" current time: ";
	
		
		ofs<<(curr->tm_hour)<<":";
		ofs<<(curr->tm_min)<<":"<<(curr->tm_sec)<<" )\n";

		ofs.write(_error.c_str(), _error.length());
		ofs.write( "\n\n", 2 );

		ofs.flush();
		ofs.close();
	}
}

void FatalError::SetLogfileName( const char *pNewLogfile )
{
	g_pLogfileName = pNewLogfile;
}

FatalError::FatalError() : _error("")
{
	Initialise("Fatal error: unknown cause");		
}

FatalError::FatalError( const char *pError ) : _error("")
{
	Initialise(pError);
}

const char *FatalError::GetError()
{
	return _error.c_str();
}

FatalError::~FatalError()
{
}


