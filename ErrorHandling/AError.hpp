#ifndef _AESTHOR_ERROR_HANDLING_HPP_
#define _AESTHOR_ERROR_HANDLING_HPP_

#include <string>
#include <fstream>
#include <ctime>

namespace AError
{

	constexpr static bool g_WriteErrorsToLogfile = true;
	
	
#ifdef DEBUG_FULL
	#define EXTENDED_DEBUG_INFO "\nThrowed in function: ",__FUNCTION__," in File: ",__FILE__," in Line: ",__LINE__
#else
	#define EXTENDED_DEBUG_INFO ""
#endif


	class FatalError
	{
		private:
			std::string _error;
			static const char *g_pLogfileName;
			
			void Initialise( const char *pErrorCause );
			void Initialise( int x )
			{
				_error+=std::to_string(x);
				Initialise("");
			}
			
			template<typename ...Arguments>void Initialise( int x, Arguments ...args)
			{
				_error+=std::to_string(x);
				Initialise(args...);
			}

			template<typename ...Arguments>void Initialise(const char *arg, Arguments ...args)
			{
				_error+=arg;
				Initialise(args...);
			}
		public:
			static void SetLogfileName( const char *pNewLogfile );
			
			FatalError();
			FatalError( const char *pErrorCause );

			template<typename ...Arguments>FatalError( Arguments... args) : _error("")
			{
				Initialise(args...);
			}
	
			const char *GetError();
	
			~FatalError();
	};
};


#endif
