#ifndef _ERROR_CHECKING_HPP_
#define _ERROR_CHECKING_HPP_

#include "../Metaprogramming/tuple.hpp"

template<int N, typename ...Arguments>struct ConditionToHave
{
	static constexpr bool value = (AMeta::SelectTupel<N,Arguments...>::ContainedType::Order==N)?ConditionToHave<N-1,Arguments...>::value:false;
};
template<typename ...Arguments>struct ConditionToHave<0,Arguments...>
{
	static constexpr bool value = (AMeta::SelectTupel<0,Arguments...>::ContainedType::Order==0)?true:false;
};

template<typename ...Arguments>struct CheckIntegrity;
template<typename ...Arguments>struct CheckIntegrity<AMeta::Tupel<Arguments...>>
{
	constexpr static bool value = ConditionToHave<AMeta::VariadicArgumentCount<Arguments...>::size-1,Arguments...>::value;
};





#endif
