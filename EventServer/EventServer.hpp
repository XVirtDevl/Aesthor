#ifndef _EVENT_SERVER_HPP_
#define _EVENT_SERVER_HPP_

#include "../Metaprogramming/tuple.hpp"
#include "../Atomic/ARessource.hpp"
#include <thread>
#include <vector>
#include <atomic>


namespace AesthorEvents
{
	using namespace AMeta;
	#include "NewEvent.hpp"
	#include "ErrorChecking.hpp"

	class Command
	{
		private:
			GeneralReceiver *_rec;
			GeneralEvent *_ev;
		public:
			Command( GeneralReceiver *rec, GeneralEvent *ev) : _rec(rec), _ev(ev){}
			void Execute()
			{
				_ev->Handle(_rec);
			}		
	};

	template<typename T,typename ...Arguments>
	class IEventServer;

	template<typename ...Arguments>
	static void ThreadStartPoint( IEventServer<Tupel<Arguments...>> *evServer )
	{
		evServer->ThreadEntryPoint();
	}

	template<typename ...Arguments>
	class IEventServer<Tupel<Arguments...>>
	{
		static_assert( CheckIntegrity<Tupel<Arguments...>>::value, "Static assert triggered, the input tuple must be totally ordered" );
		static constexpr unsigned long numberOfEvents = Tupel<Arguments...>::containedElem;

		private:
			AAtomic::Ressource<std::vector<GeneralReceiver*>*> _eventNotifierChain[ numberOfEvents ];
			AAtomic::Ressource<std::vector<Command*>*> _commandQeue;
			std::atomic<unsigned long> _jobsInCommandQeue;
			std::thread *_threads;
			bool _terminate;
			std::atomic<unsigned long> _threadCount;

		public:
			
			IEventServer() : _jobsInCommandQeue(0), _threads(nullptr), _terminate(false), _threadCount(0)
			{
			}


			void Initialise(unsigned long numThreads)
			{
				for( unsigned long i = 0; i < numberOfEvents; i++ )
				{
					_eventNotifierChain[i].SetRessource( new std::vector<GeneralReceiver*> );
				}
				_commandQeue.SetRessource( new std::vector<Command*> );
				_threads = new std::thread[numThreads];
				
				for( unsigned long i = 0; i < numThreads; i++ )
				{
					_threads[i] = std::move( std::thread( ThreadStartPoint<Arguments...>, this ) );
					_threads[i].detach();
				}
			}


			void ThreadEntryPoint()
			{
				_threadCount.fetch_add(1);	
				for(;;)
				{	
					for(;;)
					{
						if( _jobsInCommandQeue.load() != 0 )
							break;
					}
					
					AAtomic::RessourceAccessPipeline<std::vector<Command*>*> recAccess( &_commandQeue );
					if( recAccess.TrySetLock( AAtomic::LockType::LOCK_FOR_WRITE ) )
					{
						if( _jobsInCommandQeue.load() != 0 )
						{
							if(_terminate)
								break;

							_jobsInCommandQeue.fetch_sub(1);
							auto vec = recAccess.GetRessource();
							Command *x = (*vec)[0];
							vec->erase(vec->begin());
							recAccess.Unlock();
							x->Execute();
							delete x;
						}
					}
				}	
				_threadCount.fetch_sub(1);
			}

			template<int N>void RegisterForEvent( typename AMeta::SelectTupelX<N,Tupel<Arguments...>>::ContainedType::EventReceiver *eveRec )
			{
				AAtomic::RessourceAccessPipeline<std::vector<GeneralReceiver*>*> recAccess( &_eventNotifierChain[N] );
				recAccess.SetLock(AAtomic::LockType::LOCK_FOR_WRITE);
				recAccess.GetRessource()->push_back(eveRec);	
			}

			template<int N>void UnregisterFromEvent( typename AMeta::SelectTupelX<N,Tupel<Arguments...>>::ContainedType::EventReceiver *eveRec)
			{
				AAtomic::RessourceAccessPipeline<std::vector<GeneralReceiver*>*> recAccess( &_eventNotifierChain[N]);
				recAccess.SetLock(AAtomic::LockType::LOCK_FOR_WRITE);
				int i = 0;
				std::vector<GeneralReceiver*> *vecPtr = recAccess.GetRessource();
				for( auto &x : *vecPtr)
				{
					if( x == eveRec )
						break;
					i++;
				}

				if( i < numberOfEvents)
				{
					vecPtr->erase(vecPtr->begin()+i);
				}
			}
			

			template<int N>void TriggerEventSync( typename AMeta::SelectTupelX<N,Tupel<Arguments...>>::ContainedType::Event *eventTo)
			{
				AAtomic::RessourceAccessPipeline<std::vector<GeneralReceiver*>*> recAccess( &_eventNotifierChain[N] );
				recAccess.SetLock(AAtomic::LockType::LOCK_FOR_READ);
				for( auto &i : *recAccess.GetRessource())
				{
					eventTo->Handle(i);
				}
			}

			template<int N>void TriggerEventAsync( typename AMeta::SelectTupelX<N,Tupel<Arguments...>>::ContainedType::Event *eventTo )
			{
				AAtomic::RessourceAccessPipeline<std::vector<GeneralReceiver*>*> recAccess( &_eventNotifierChain[N] );
				AAtomic::RessourceAccessPipeline<std::vector<Command*>*> commAccess( &_commandQeue );

				recAccess.SetLock(AAtomic::LockType::LOCK_FOR_READ);
				commAccess.SetLock( AAtomic::LockType::LOCK_FOR_WRITE);
				
				unsigned long x = 0;
				for( auto &i : *recAccess.GetRessource())
				{
					commAccess.GetRessource()->push_back( new Command(i, eventTo ) );
					x++;
				}
				eventTo->SetEstimatedWorkCount( recAccess.GetRessource()->size());
				_jobsInCommandQeue.fetch_add( x );
			}

			~IEventServer()
			{
				_terminate = true;
				_jobsInCommandQeue.fetch_add(100);				

				for( unsigned long i = 0; i < numberOfEvents; i++)
				{	
					AAtomic::RessourceAccessPipeline<std::vector<GeneralReceiver*>*> al( &_eventNotifierChain[i]);
					al.SetLock(AAtomic::LockType::LOCK_FOR_WRITE);	
					auto res = al.GetRessource();
					if( res != nullptr )
					{
						delete res;
						_eventNotifierChain[i].SetRessource(nullptr);
					}	
				}

				for(;;)
				{
					if( _threadCount.load() == 0 )
						break;
				}
				
				AAtomic::RessourceAccessPipeline<std::vector<Command*>*> al( &_commandQeue );
				al.SetLock( AAtomic::LockType::LOCK_FOR_WRITE );
				auto res = al.GetRessource();	
				if( res != nullptr )
				{
					delete res;
					_commandQeue.SetRessource(nullptr);
				}
				if( _threads != nullptr )
				{
					delete []_threads;
					_threads = nullptr;
				}
			}
		
	};
}

#endif
