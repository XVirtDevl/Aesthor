#ifndef _NEW_EVENT_HPP_
#define _NEW_EVENT_HPP_

#include "../Metaprogramming/tuple.hpp"

class GeneralReceiver
{
};

class GeneralEvent
{
	public:
		virtual void Handle( GeneralReceiver *rec ) = 0;
};
	

template<int NWhichOrder,typename ...Arguments>class NewEvent
{
	public:
	static constexpr unsigned long Order = NWhichOrder;

	class EventReceiver : public GeneralReceiver 
	{
		public:
			virtual void Receive(Arguments ...args) = 0;
	};
		
	class Event : public GeneralEvent
	{
		private:
			AMeta::Tupel<Arguments...> funcArgs;
			std::atomic<unsigned long> _estimatedFinishes;
		public:
			Event(Arguments ...args) : funcArgs(args...)
			{}
				
			void SetEstimatedWorkCount( unsigned long x )
			{
				_estimatedFinishes = x;
			}

				

			void SyncWithThreads()
			{
				for(;;)
					if( _estimatedFinishes.load() == 0 )
						break;
			}
				
			void Handle(GeneralReceiver *rec) override
			{
				AMeta::UnrollThings< AMeta::VariadicArgumentCount<Arguments...>::size >::Unroll((EventReceiver*)rec,&EventReceiver::Receive,funcArgs);
				_estimatedFinishes.fetch_sub(1);
			}
	};
};



#endif
