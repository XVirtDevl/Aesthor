#include "EventServer.hpp"
#include <iostream>
#include <unistd.h>
using namespace std;


using namespace AesthorEvents;


typedef NewEvent<0, int> PrintInteger;
typedef NewEvent<1, const char *> PrintString;

typedef Tupel<PrintInteger,PrintString> MyTup;


class X : public PrintInteger::EventReceiver
{
	public:
		void Receive(int x) override
		{
			cout<<"Yolo for async x"<<endl;			
		}
};


class Y : public PrintInteger::EventReceiver
{
	public:
		void Receive(int y) override
		{
			sleep(10);
			cout<<"Yolo y reached"<<endl;
		}
};

int main()
{
	IEventServer<MyTup> eventServer;
	eventServer.Initialise(2);

	X x;
	Y y;

	eventServer.RegisterForEvent<PrintInteger::Order>(&x);
	eventServer.RegisterForEvent<PrintInteger::Order>(&y);

	PrintInteger::Event ev(1000);
	
	eventServer.TriggerEventAsync<PrintInteger::Order>( &ev );

	ev.SyncWithThreads();

	return 0;
}
