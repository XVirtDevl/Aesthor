#include "OSAbstractionImp.hpp"

/*
	OSAFDShared function definition!
*/
IOSAFilesystem *IOSAFilesystem::GetFilesystem(unsigned long long memory_limit)
{
	static OSAFilesystem _sys(memory_limit);

	return &_sys;
}

const char *OSAFDShared::GetFullFilepath() const
{
	return const_cast<const char*>(pFullFilepath.get());
}
const char *OSAFDShared::GetFilename() const
{
	return const_cast<const char*>(pFilename);
}

const char *OSAFDShared::GetFileExt() const
{
	return const_cast<const char*>(pFileExt);
}

void OSAFDShared::ReserveMemCopyStr( std::unique_ptr<const char> &StringToFill, const char *pToCopyStr )
{
	int FilenameLen;

	pFilename = pToCopyStr;
	for( FilenameLen = 0; pToCopyStr[ FilenameLen ] != '\0'; FilenameLen++ )
	{
		if( pToCopyStr[FilenameLen] == '/' )
			pFilename = &pToCopyStr[ FilenameLen+1 ];
		else if( pToCopyStr[FilenameLen] == '.' )
			pFileExt = &pToCopyStr[ FilenameLen+1 ];
	}

	if( pFileExt == nullptr )
		pFileExt = &pToCopyStr[FilenameLen];

	FilenameLen++;

	char *pFilenameBuffer = new char[ FilenameLen ];

	for( int i = 0; i < FilenameLen; i++ )
		pFilenameBuffer[i] = pToCopyStr[i];
	
	StringToFill.reset(pFilenameBuffer);
}

OSAFDShared::OSAFDShared() :  pFilename{nullptr}, pFileExt{nullptr}, pFullFilepath{nullptr} 
{}


OSAFDShared::~OSAFDShared()
{	
}

void OSAFDShared::Initialise( const char *pFile)
{
	ReserveMemCopyStr( pFullFilepath, pFile );
}




/*
	OSAFile function defines!
*/
OSAFile::OSAFile( const char *pFile) : OSAFDShared{},Filesize(0)
{
	OSAFDShared::Initialise(pFile);
}

bool OSAFile::IsOpened() const
{
	return fileStream.is_open();
}

unsigned long long OSAFile::GetFilesize() const
{
	return Filesize;
}

void OSAFile::Read( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize )
{
	if( !fileStream.is_open() )
		throw AError::FatalError{"Trying to read from file without opening it!", EXTENDED_DEBUG_INFO};

	fileStream.seekg(Offset, std::ios::beg );
	
	if( fileStream.eof())
	{
		BufferSize = -1;
		return;
	}

	fileStream.read( pBuffer, BufferSize );

	BufferSize = (BufferSize+Offset)-fileStream.tellg();
}

void OSAFile::Write( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize )
{
	if( !fileStream.is_open() )
		throw AError::FatalError{"Trying to attempt to write while file is still not opened",EXTENDED_DEBUG_INFO};

	fileStream.seekp(Offset, std::ios::beg);

	fileStream.write( pBuffer, BufferSize );

	BufferSize = (BufferSize+Offset)-fileStream.tellp();
}

bool OSAFile::IsDirectory() const
{ return false; }

void OSAFile::OpenFile()
{
	fileStream.open( pFullFilepath.get(), std::ios::in );

	if( fileStream.fail() )
		throw AError::FatalError{"File: ",pFullFilepath.get()," does not exist",EXTENDED_DEBUG_INFO};

	fileStream.close();
	fileStream.open( pFullFilepath.get(), std::ios::in|std::ios::out);
}
	
void OSAFile::CloseFile()
{
	if( fileStream.is_open() )
		fileStream.close();
}

OSAFile::~OSAFile()
{
}









//TODO



IOSAGenericFile *OSAFilesystem::OpenFile( const char *FullFilePath )
{
	if( _openFiles.size() != 0 )
	{
		for( auto &i : _openFiles )
		{
			if( strcmp( i->GetFullFilepath(), FullFilePath ) == 0)
			{ return i; }
		}
	}

	IOSAGenericFile *file = new OSAFile(FullFilePath);
	try
	{
		file->OpenFile();
		_openFiles.push_back(file);
		return file;
	}
	catch( AError::FatalError &err)
	{
		delete file;
		throw;	
	}
}

IOSAGenericFile *OSAFilesystem::OpenDirectory( const char *FullDirPath )
{
	return nullptr;
}

OSAFilesystem::OSAFilesystem( unsigned long max_mem )
{
	_maxMemory = max_mem;
}


IOSAGenericFile *OSAFilesystem::CreateFile(const char *FullFilePath)
{
	return nullptr;	
}

IOSAGenericFile *OSAFilesystem::CreateDirectory( const char *FullDirPath )
{
	return nullptr;
}

void OSAFilesystem::WriteAllChangesToDisk()
{
}

OSAFilesystem::~OSAFilesystem()
{
	for( auto &i : _openFiles )
		delete i;
}













/*
OSADirectory( const char *pDirectoryName )
{
}

unsigned long long GetFilesize() const override
{
}
		
bool IsDirectory() const override
{
}
void Read( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) override
{
}
void Write( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) override
{
}*/







