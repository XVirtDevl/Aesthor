#ifndef _OPERATING_SYSTEM_ABSTRACTION_LAYER_IMP_HPP_
#define _OPERATING_SYSTEM_ABSTRACTION_LAYER_IMP_HPP_

#include "OSAbstraction.hpp"
#include "../ErrorHandling/AError.hpp"
#include <vector>
#include <fstream>
#include <memory>
#include <thread>
#include <cstring>


class OSAFDShared : public IOSAGenericFile
{
	protected:
		const char* pFilename;
		const char *pFileExt;
		std::unique_ptr<const char> pFullFilepath;

		void ReserveMemCopyStr( std::unique_ptr<const char> &StringToFill, const char *pToCopyStr );
	public:
		OSAFDShared();
		virtual const char *GetFilename() const override;
		virtual const char *GetFullFilepath() const override;
		virtual const char *GetFileExt() const override;
		void Initialise( const char *pFile);
		virtual ~OSAFDShared();
};

class OSAFile : public OSAFDShared
{
	private:
		unsigned long Filesize;
		std::fstream fileStream;

	public:
		OSAFile( const char *pFullFilepath );
		
		unsigned long long GetFilesize() const override;
		bool IsDirectory() const override;
		bool IsOpened() const override;

		void OpenFile() override;
		void Read( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) override;
		void Write( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) override;	
		void CloseFile() override;
		
		~OSAFile();
};

class OSADirectory : public OSAFDShared
{
	private:
		unsigned long numEntrys;


	public:
		OSADirectory( const char *pDirectoryName );

		unsigned long long GetFilesize() const override;
		
		bool IsDirectory() const override;
		void Read( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) override;
		void Write( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) override;
};


class OSAFilesystem : public IOSAFilesystem
{
	protected:
		unsigned long _maxMemory;
		std::vector<IOSAGenericFile*> _openFiles;
	public:
		OSAFilesystem( unsigned long max_mem );
		virtual IOSAGenericFile *CreateFile(const char *FullFilePath) override;
		virtual IOSAGenericFile *CreateDirectory( const char *FullDirPath ) override;

		virtual IOSAGenericFile *OpenFile( const char *FullFilePath ) override;
		virtual IOSAGenericFile *OpenDirectory( const char *FullDirPath ) override;
	
		virtual void WriteAllChangesToDisk() override;
		~OSAFilesystem();
};


#endif
