#ifndef _OPERATING_SYSTEM_ABSTRACTION_LAYER_HPP_
#define _OPERATING_SYSTEM_ABSTRACTION_LAYER_HPP_

//Interface for the classes OSFile and OSDirectory!
class IOSAGenericFile
{
	public:
		virtual const char *GetFilename() const = 0;
		virtual const char *GetFullFilepath() const = 0;
		virtual const char *GetFileExt() const = 0;

		virtual unsigned long long GetFilesize() const = 0;
		virtual bool IsDirectory() const = 0;
		virtual bool IsOpened() const = 0;

		virtual void OpenFile() = 0;
		virtual void Read( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) = 0;
		virtual void Write( unsigned long long Offset, char *pBuffer, unsigned long &BufferSize ) = 0;
		virtual void CloseFile() = 0;

		virtual ~IOSAGenericFile(){};	
		char *ReadToPtr( IOSAGenericFile** filePtr )
		{
			return reinterpret_cast<char*>(filePtr);
		}
};



class IOSAFilesystem
{
	public:
		static IOSAFilesystem *GetFilesystem(unsigned long long memory_limit);

		virtual IOSAGenericFile *OpenFile( const char *FullFilePath ) = 0;
		virtual IOSAGenericFile *OpenDirectory( const char *FullDirPath ) = 0;

		virtual IOSAGenericFile *CreateFile(const char *FullFilePath) = 0;
		virtual IOSAGenericFile *CreateDirectory( const char *FullDirPath ) = 0;

		virtual void WriteAllChangesToDisk() = 0;
};

#endif
