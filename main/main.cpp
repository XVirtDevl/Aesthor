#include "../ErrorHandling/AError.hpp"
#include "../Atomic/ARessource.hpp"
#include "../Atomic/Command.hpp"
#include "../DisplayServer/DisplayServer.hpp"
#include "../OSLayer/OSAbstraction.hpp"
#include "../InputServer/InputServer.hpp"
#include <unistd.h>


int main(int argc, char **argv)
{
	IDisplayDevice *dev = IDisplayDevice::GetPrimaryDisplay();
	dev->InitialiseDisplay();

	IWindow *w = dev->ConstructWindow( 0, 0, dev->GetXResolutionInChars(), dev->GetYResolutionInChars() );	
	IWindow *stateWin = w->AllocateStatewindow(1);
	stateWin->SetWindowAttributes( 0, 7 );
	stateWin->FlushToGraphicsPipeline();

	w->SetColor( 1, 2);
	w->DrawLine("Hallo Welt");

	w->FlushToGraphicsPipeline();
 
	IInputServer *in = IInputServer::CreateInputServer();
	in->Start();
	
	return 0;
}
