#ifndef _AESTHOR_TUPLE_HPP_
#define _AESTHOR_TUPLE_HPP_


#include "meta.hpp"


namespace AMeta
{

	template<typename ...AllArguments>
	struct Tupel
	{
		static_assert( AMeta::AlwaysFalse<AllArguments...>::value, "A Tupel with zero members does not contain any space!");
	};


	
	template<int N,typename Head, typename ...Tail>
	struct SelectTupel : SelectTupel<N-1,Tail...>
	{};
	template<typename Head, typename ...Tail>
	struct SelectTupel<0,Head,Tail...>
	{
		using ContainedType = Head;
		using value = Tupel<Head,Tail...>;
	};


	template<typename T, typename ...Args>
	struct Tupel<T,Args...> : Tupel<Args...>
	{
			constexpr static unsigned long containedElem = AMeta::VariadicArgumentCount<T,Args...>::size;

		

			T _val;

			Tupel(T newVal, Args ...arguments) : Tupel<Args...>::Tupel(arguments...)
			{
				_val = newVal;
			}

			template<int N>typename SelectTupel<N,T,Args...>::ContainedType get() const
			{
				return SelectTupel<N,T,Args...>::value::_val;
			}

			template<int N>void set( typename SelectTupel<N,T,Args...>::ContainedType newVal)
			{
				SelectTupel<N,T,Args...>::value::_val = newVal;		
			}

	};

	template<typename T>
	struct Tupel<T>
	{
		T _val;
		Tupel( T value)
		{
			_val = value;
		}	
		
		template<int N>T get() const
		{
			return _val;
		}



	};	

	
	template< int NElem >
	struct UnrollThings
	{
		template<typename ClassName, typename ...ClassFunctionArguments, typename ...TupleElements, typename ...UnrolledArgs>
		static void Unroll( ClassName *obj, 
					void (ClassName::*cfunc)(ClassFunctionArguments...),
					const Tupel<TupleElements...>& t, 
					UnrolledArgs ...args)
		{
			UnrollThings<NElem-1>::Unroll(obj,cfunc,t,t.template get<(NElem-1)>(),args...);
		}
	};

	template<>
	struct UnrollThings<0>
	{
		template<typename ClassName, typename ...ClassFunctionArguments, typename ...TupleElements, typename ...UnrolledArgs>
		static void Unroll( ClassName *obj, void (ClassName::*cfunc)(ClassFunctionArguments...),const Tupel<TupleElements...>& tup, UnrolledArgs ...args)
		{
			(obj->*cfunc)(args...);	
		}
	};




	template<int N, typename Head, typename ...Tail>struct SelectTupelX;
	template<int N, typename Head, typename ...Tail>struct SelectTupelX<N,Tupel<Head,Tail...>> : SelectTupelX<N-1,Tupel<Tail...>>
	{
	};
	template<typename Head, typename ...Tail>struct SelectTupelX<0,Tupel<Head,Tail...>>
	{
		using ContainedType = Head;
	};
};













#endif
