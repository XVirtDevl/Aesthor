#ifndef _METAPROGRAMMING_AESTHOR_HPP_
#define _METAPROGRAMMING_AESTHOR_HPP_

namespace AMeta
{
	template<bool C, typename T, typename F>struct Conditional
	{
		using Type = T;
	};

	template<typename T, typename F>struct Conditional<false,T,F>
	{
		using Type = F;
	};


	template< typename T>struct IsPointer
	{
		constexpr static bool value = false;
		using Type = T;
	};

	template< typename T>struct IsPointer<T*>
	{
		constexpr static bool value = true;
		using Type = T;
	};

	template<typename ...AllArgs>struct VariadicArgumentCount
	{
		static constexpr unsigned long size = 0;
	};

	template<typename Head, typename ...Tail>
	struct VariadicArgumentCount<Head,Tail...>
	{
		static constexpr unsigned long size = 1+VariadicArgumentCount<Tail...>::size;
	};

	template<typename Head>
	struct VariadicArgumentCount<Head>
	{
		static constexpr unsigned long size = 1;
	};


	template<typename ...Args>
	struct AlwaysFalse
	{
		constexpr static bool value = false;
	};

	template<typename ...Args>
	struct AlwaysTrue
	{
		constexpr static bool value = true;
	};
};


#endif
