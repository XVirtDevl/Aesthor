ErrorHandling:
	g++ -std=c++11 -Wall -O3 ErrorHandling/AError.cpp -c -o bin/aerror.o

test_errorhandling: ErrorHandling
	g++ -std=c++11 -O3 ErrorHandling/test.cpp -o bin/test.o bin/aerror.o -IErrorHandling
	bin/test.o
	rm bin/test.o
	rm bin/aerror.o

test_atomic:
	g++ -std=c++11 -O3 Atomic/test.cpp -o bin/test.o -IAtomic -pthread 
	bin/test.o
	rm bin/test.o

Filesystem:
	g++ -std=c++11 -Wall -O3 OSLayer/OSAbstractionImp.cpp	-c -o bin/filesystem.o -IOSLayer

test_filesystem : ErrorHandling Filesystem
	g++ -std=c++11 -Wall -O3 OSLayer/main.cpp -o bin/test.o -IOSLayer bin/filesystem.o bin/aerror.o
	bin/test.o
	rm bin/test.o

DisplayServer:
	g++ -Wall -std=c++11 -O3 DisplayServer/DisplayServerNCurses.cpp -c -o bin/dsp.o -IDisplayServer -lncurses

test_displayserver: DisplayServer	
	g++ -Wall -std=c++11 -O3 DisplayServer/test.cpp -o bin/test.o bin/dsp.o -IDisplayServer -lncurses
	bin/test.o
	rm bin/test.o
	rm bin/dsp.o




test_eventserver: 
	g++ -Wall -std=c++11 -O3 EventServer/main.cpp -o bin/test.o -IEventServer -pthread
	bin/test.o
	rm bin/test.o


InputServer: 
	g++ -Wall -c -std=c++11 -O3 InputServer/NCInputServer.cpp -o bin/inputserver.o -IEventServer -IInputServer -pthread

main.o: ErrorHandling Filesystem DisplayServer Atomic InputServer
	g++ -std=c++11 -o bin/main.o main/main.cpp -O3 bin/dsp.o bin/aerror.o bin/filesystem.o bin/inputserver.o bin/eventserver.o -lncurses -pthread

test_main: main.o
	bin/main.o

.PHONY: ErrorHandling Atomic Filesystem DisplayServer InputServer
